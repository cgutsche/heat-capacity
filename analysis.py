# %%
from cProfile import label
import numpy as np
import scipy as sp
import pandas as pd
from scipy.integrate import simpson
import matplotlib.pyplot as plt
from scipy.stats import linregress
from uncertainties import ufloat
from uncertainties.umath import sqrt
from scipy.stats import sem
# %%
R = ufloat(7.1,0.1) #resistance of heating plate
I_heating_theory = sqrt(20/R)
V_heat_theory = sqrt(R*20)

V_heat = ufloat(12, 0.5)
I_heating = ufloat(1.67,0.05)
# Water experiment
volume_water = ufloat(50, 1)*1e-6
m_water = 997 * volume_water
m_water # 0.04985+/-0.000997
dm = 1e-3 # 1 division on scale
m_al = ufloat(0.474, dm)
m_cu = ufloat(1.546, dm)
# %%
water = pd.read_csv("water measurement.csv")
# water = water.iloc[2045:, :]
# Replace to drop rows or columns infinite values
def cleanup(df):
    df = df.iloc[1:,:]
    df = df.astype(float)
    df = df.replace([np.inf, -np.inf], np.nan).dropna(axis=0)
    df["Time"] = df["Time"] + 1000
# because there are some large spikes we are taking the rolling median over 20 datapoints
    df.iloc[:,[1,2]] = df.iloc[:,[1,2]].rolling(20).median() # report
    df = df.dropna(axis=0)
    return df

water = cleanup(water) # report

def plot(df, filename = "test", title = None):
    plt.plot(df["Time"],df["Channel A"], label="chanel A")
    plt.plot(df["Time"],-df["Channel B"], label="chanel B")
    plt.xlabel("time in s")
    plt.ylabel("Voltage in mV for ChA and V for ChB")
    plt.title(title)
    plt.legend()
    plt.savefig(f"{filename}.pdf")

plot(water, "water", "water measurement, no sample")
water
def get_capacity(I_heating, V_heat, df):
    t = np.array(df.iloc[:,0].astype(float))
    ch_A = np.array(df.iloc[:,1].astype(float))
    ch_B = -np.array(df.iloc[:,2].astype(float))
    T = ch_A/0.4
    V_heat = ufloat(np.mean(ch_B[ch_B>1]), sem(ch_B[ch_B>1])+ 0.024e-6) # report
    print("V_heat",V_heat)
    T_when_heating = ch_A[ch_B>1]/0.4
    time_heat = t[ch_B>1]
    DT = T_when_heating[-1] - T_when_heating[0]
    # resolution of picoscope is 0.024 mV
    # channel A is allready in mV
    err_T = 0.024/0.4
    DT = ufloat(DT, 2*err_T) #report
    print("DT",DT)
    t_startheat = time_heat[0]
    t_endheat = time_heat[-1]
    ts = t[0]
    tg = t[-1]
    t_tail = t>tg-60
    t_after_heat = t[t_tail] #last 60 seconds, report
    T_after_heat = ch_A[t_tail]/0.4
    Dt = t_endheat - t_startheat
    T1 = T[0]
    TG = T[-1]
    F = simpson(T-T1,t)
    F = ufloat(F, (tg-ts)*err_T) #report
    T_dot = linregress(t_after_heat,T_after_heat).slope #report
    print("T_dot * F", T_dot * F)
    print("I_heating*V_heat", I_heating*V_heat)
    print("(t_endheat - t_startheat) in min", (t_endheat - t_startheat)//60 , (t_endheat - t_startheat)%60)
    C_tot = I_heating*V_heat*(t_endheat - t_startheat)*DT/(DT**2- T_dot * F)

    C_naiive = I_heating*V_heat*(t_endheat - t_startheat)/DT
    K = - C_tot*T_dot/DT
    Q_L = K*F
    print("Q_L", Q_L)
    print("C_tot, C_naiive", C_tot, C_naiive)
    return C_tot

C_water = get_capacity(I_heating, V_heat, water)
C_water
# %%

copper_raw = pd.read_csv("copper.csv")
copper = cleanup(copper_raw)
plot(copper, "copper", "Temperature Measurement for Copper")
C_copper_tot = get_capacity(I_heating, V_heat, copper)
m_atom_cu = ufloat(63.5463, 0.0005) #in units

def get_molar_cap(m_cu, C_copper_tot, m_atom_units):
    atomic_mass = 1.66054e-27
    m_atom_copper = m_atom_units*atomic_mass # "Standard Atomic Weights: Copper". CIAAW. 1969.https://www.ciaaw.org/copper.html
    avocado_number = 6.022e23
    C_copper = C_copper_tot - C_water
    n = m_cu/m_atom_copper/avocado_number
    C_m_copper = C_copper/n
    return C_m_copper

C_m_copper = get_molar_cap(m_cu, C_copper_tot, m_atom_cu)

print("molar capacity copper",C_m_copper) # reference: 
C_cu_ref = 24.440 # J/(mol·K)
# output: 22.9+/-1.0
# %%
al_raw = pd.read_csv("Aluminium.csv")
al = cleanup(al_raw)
plot(al,"alu", "Temperature Measurement of Aluminium")
C_al_tot = get_capacity(I_heating,V_heat,al)
m_atom_al = ufloat(26.9815384, 0.000005)
C_al_ref = 24.20 #J/(mol·K)
C_m_al = get_molar_cap(m_al, C_al_tot, m_atom_al)
print("molar capacity aluminium", C_m_al) # 21.5+/-1.0
# %%
d1 = (C_m_copper - C_cu_ref)/C_cu_ref
d2 = (C_m_al - C_al_ref)/C_al_ref
print(d1*100)
print(d2*100)